import pickle
from collections import Counter #tokenization
import keras #ML
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import Embedding
from keras.layers import LSTM

""" --- create a summary headline for a BBC news article --- """
# C:\Users\Dkettemann-intern\AppData\Local\Continuum\anaconda3\Scripts
print('starting...')

filename = 'articles.pkl'

infile = open(filename, 'rb') #rb
#   TODO: create and use keywords for better results
heads, desc, keys = pickle.load(infile)
i = 0
print(heads[i])
print(desc[i])
print(keys)
infile.close()


""" tokenize text """


def get_vocab(lst):
    # vocab_count, vocab = Counter(w for txt in lst for w in txt.split()) # that may work in python 2 """
    """ go through every text in lst and then through every word """
    vocab = Counter(w for txt in lst for w in txt.split())
    vocab_count = len(vocab)
    return vocab, vocab_count


vocab, vocabCount = get_vocab(heads+desc)
print(vocab)
print(vocabCount)

# initialize an embedding matrix with the tokenized vocab from the training data with random numbers
# then copy the matching GloVe weights into the EM
# for every word outside the EM find the closest word inside by measuring the cosine distance of GloVe Vectors

""" Stacked LSTM RNN coming """

"""
    match vocab and pre-learned embeddings from GloVe

    represent words with numbers (embeddings)
    using a count based approach to find matching words - one at a time (GloVe)
    create an occurrence matrix
    count how frequently a word (row) appears in a context (column)
    also factorizes a matrix to get a lower dim matrix
    
    sequence to sequence model
    basic model description:
      two recurrent networks (RNNs): encoder and decoder
      1. create an encoded sequence of an input
      2. generate an output sequence by decoding the encoded sequence
"""

""" set the pre-trained embeddings as the first layer's weight """

"""
def build_model(embedding):
    model = Sequential()
    model.add(Embedding(weights=[embedding], name='embedding_1'))
    for i in range(3):
        lstm = LSTM()



"""