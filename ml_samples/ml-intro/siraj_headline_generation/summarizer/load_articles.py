import os
import glob
import pickle

print('starting...')
path = './bbcsport/athletics/'

filename = 'articles.pkl'
outfile = open(filename, 'wb')

headings = []
content = []
keywords = []


i = 0
for filename in glob.glob(os.path.join(path, '*.txt')):
    print('\n' + '========================= ' + str(i) + ' =========================')
    f = open(filename)
    first_line = f.readline()
    print('heading: ' + ' \n ' + first_line)
    print('article: ')
    desc = ''
    headings.append(first_line)
    for line in f:
        if line == '\n':
            continue
        desc = desc + line

    content.append(desc)
    print(desc)
    f.close()
    i += 1

try:
    i = 0
finally:
   print('success')

pickle.dump([headings, content, keywords], outfile)
outfile.close()
