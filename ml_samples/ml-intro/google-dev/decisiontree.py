# 1. import dataset
# 2. train a classifier
# 3. predict value for a new flower
# 4. visualize the tree

import numpy as np
from sklearn import tree
from sklearn.datasets import load_iris

iris = load_iris()
print(iris.feature_names)
# target contains the labels
print(iris.target_names)
print(iris.data[0])
print(iris.target[0])

for i in range(len(iris.data)):
    print("Example %d: label %s, features %s" %
          (i, iris.data[i], iris.target[i]))

test_idx = [0, 50, 100]

# training the data
train_target = np.delete(iris.target, test_idx)
train_data = np.delete(iris.data, test_idx, axis=0)

# testing the data
test_target = iris.target[test_idx]
test_data = iris.data[test_idx]

clf = tree.DecisionTreeClassifier()
clf.fit(train_data, train_target)

print(test_target)
print(clf.predict(test_data))

# visualization
# too much graphviz needed
