from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn import datasets
from sklearn import tree
iris = datasets.load_iris()

X = iris.data
y = iris.target


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5)

decisiontree_classifier = tree.DecisionTreeClassifier()

knn_classifier = KNeighborsClassifier()

decisiontree_classifier.fit(X_train, y_train)
knn_classifier.fit(X_train, y_train)

predictions1 = decisiontree_classifier.predict(X_test)
predictions2 = knn_classifier.predict(X_test)

print(accuracy_score(y_test, predictions1))
print(accuracy_score(y_test, predictions2))
