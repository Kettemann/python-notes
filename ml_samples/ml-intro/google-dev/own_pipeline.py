# has to implement 2 methods
# 1. fit to train with training data
# 2. predict to test with testing data
import random
from scipy.spatial import distance
from sklearn import tree
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier


def euc(a, b):
    return distance.euclidean(a, b)


class ScrappyKNN():
    def fit(self, X_train, y_train):
        self.X_train = X_train
        self.y_train = y_train

    def predict(self, X_test):
        predictions = []
        for row in X_test:
            label = self.closest(row)
            predictions.append(label)
        return predictions

    def closest(self, row):
        best_dist = euc(self.X_train[0], row)
        best_index = 0
        for i in range(1, len(self.X_train)):
            dist = euc(X_train[i], row)
            if dist < best_dist:
                best_dist = dist
                best_index = i
        return self.y_train[best_index]


iris = datasets.load_iris()

X = iris.data
y = iris.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5)

decisiontree_classifier = tree.DecisionTreeClassifier()

knn_classifier = ScrappyKNN()

decisiontree_classifier.fit(X_train, y_train)
knn_classifier.fit(X_train, y_train)

predictions1 = decisiontree_classifier.predict(X_test)
predictions2 = knn_classifier.predict(X_test)

print(accuracy_score(y_test, predictions1))
print(accuracy_score(y_test, predictions2))
