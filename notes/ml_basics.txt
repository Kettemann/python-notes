===================================================================================
						        Definitions
===================================================================================

classifier: takes some data as an input and assigns a label to it as output

supervised learning: automatically create a classifier by studying examples

features: measurements
 -> a good feature makes it easy to discriminate between subjects of interest

example: each row of training data

label: identifies the type of data (like spam or no spam for emails)



/******     Other General Terms     ******/

Language Modelling
 • assigning the probability for the next words and sentences in a language
 • popular models such as BERT or GPT are trained on large datasets and also optimized for transfer learning
 • 

NLP Transformer
 • replace RNNs for sequence prediction
 • uses the semantic meaning of a word (available through the word embedding / vector representation)
   and the position in the sentence for a prediction (also as a vector)
-> input sentence matrix and input positional matrix --> output probability/ies
