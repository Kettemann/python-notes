## Intro to Deep Learning
https://www.youtube.com/watch?v=vOppzHpvTiQ&list=PL2-dafEMk2A7YdKv4XfKpfbTH5z6rEEj3

#### Traditional Programming vs ML
TP is about defining the steps t reach a goal. 
ML is about defining the goal and letting the program find the way itself

#### Learning Strategies
##### Supervised Learning: 
data and labels are given
e.g. feedback after every move in chess

##### Unsupervised Learning:
data without labels
no feedbackat all, structures have to be differentiated just from the differences in data

##### Reinforcement Learning:
data without labels, but feedback after achieving a goal (e.g. beating humans at chess)

#### ML Models
##### Linear Regression:
Find the relationship between an independent and a dependent variable
```
from sklearn import linear_model
reg = linear_model.LinearRegression()
reg.fit(x_values, y_values)
```

#### General Pattern
##### Tokenization:
split text up into smaller tokens (Merkmale -> words, phrases or whole sentences)  
count the number of times each token shows up (**Bag of Words Model**)


#### Artificial Neural Networks
##### Intuition of ANNs
biologically inspired network of artificial neurons

###### McCulloch Pitts Neuron
DataStructure with binary Input, sum the Inputs and if the 
Output overcomes a Treshold it fires a 1, else a 0

###### Rosenblatt Learning:  Perceptron
Single Layer Feed Forward NN  
-> Input Weights and Changes: weights recalculated are with each Iteration (-->Learning)  
error minimization: calculate the error and update the weights accordingly (**Gradient Descent**)
Cost Function: C = Σ 1/2*(ŷ-y)^2
Backpropagation: when the error is backpropagated through the neural network and the weights are adjusted
proportional to the difference  
Also called batch gradient descent, because the weights get updated after running through the whole 
batch of examples  
(functionality: https://www.youtube.com/watch?v=IHZwWFHWa-w  
implementation: https://machinelearningmastery.com/implement-backpropagation-algorithm-scratch-python/)  
Stochastic Gradient Descent: consider the possibility of local minimums in the cost function  
run one example at a time and then adjust the weights
###### activation functions:
with x being the weighted sum of all inputs  

Treshold:   x >= 0 then Φ(x) = 1 else Φ(x) = 0  
Sigmoid:    Φ(x) = 1 / (1+e^-x) ( ==> smooth)  
Rectifier:  Φ(x) = max(x,0) (kinky line)  
Hyperbolic Tangent (tanh): Φ(x) = (1 - e^-2x)/(1 + e^-2x) (==> like sigmoid but from -1 to 1)  
##### How to build ANNs
##### predict a single outcome
##### Performance Validation (k-fold CV)
##### tackle Overfitting
##### Parameter Tuning