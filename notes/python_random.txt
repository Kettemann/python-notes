===================================================================================
						        Python Useful Things
===================================================================================
 - make an executable file from a python script:
https://www.youtube.com/watch?v=lOIJIk_maO4

/******     print an objects attributes     ******/
print(dir(object))


===================================================================================
						        Datastructures
===================================================================================
/******     Lists     ******/
x = [3, 6, 9, 12]
print(x)



/******     Arrays     ******/
y = array([3, 6, 9, 12])
y/3.0
print(y)

 • good for arithmetic functions



/******     Tuples     ******/
pi_tuple = (3, 1, 4, 1, 5, 9)

 • Tuple Values in a tuple can't change like lists
 • tuple to list
    new_tuple = list(pi_tuple)



/******     Dictionary / Map     ******/
sample = {1: 500, 2: 1000, 3: 2000, 4: 4000}
print(sample[3])

 • simple key value pairs


